package me.rokas.wotnode.model

data class TemperatureRecord(
    var deviceIdentifier:String,
    var temperature: Double
)