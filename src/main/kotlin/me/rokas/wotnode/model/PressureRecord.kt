package me.rokas.wotnode.model

import java.io.Serializable

data class PressureRecord(
    var deviceIdentifier:String,
    var pressure: Double
):Serializable