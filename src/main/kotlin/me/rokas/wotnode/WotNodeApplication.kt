package me.rokas.wotnode

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class WotNodeApplication

fun main(args: Array<String>) {
    runApplication<WotNodeApplication>(*args)
}
