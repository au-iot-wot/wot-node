package me.rokas.wotnode.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class QueueConfiguration {

    @Bean
    fun temperatureQueue(): Queue{
        return Queue("temperature")
    }

    @Bean
    fun pressureQueue(): Queue{
        return Queue("pressure")
    }

}