package me.rokas.wotnode.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import me.rokas.wotnode.model.PressureRecord
import me.rokas.wotnode.model.TemperatureRecord
import me.rokas.wotnode.repository.WeatherRepository
import org.slf4j.LoggerFactory
import org.springframework.amqp.AmqpException
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import java.util.*

@Controller
class WeatherService(val temperatureQueue:Queue,
                     val pressureQueue:Queue,
                     val rabbitClient:RabbitTemplate,
                     val weatherRepository: WeatherRepository) {

    private val id = UUID.randomUUID();
    private val logger = LoggerFactory.getLogger(javaClass)
    @Scheduled(fixedDelay = 30000)
    fun sendTemperature(){
        try {
            val temp = TemperatureRecord(id.toString(),weatherRepository.getTemperature());
            logger.info("Sending $temp to ${temperatureQueue.name}")
            rabbitClient.convertAndSend(temperatureQueue.name, jacksonObjectMapper().writeValueAsString(temp))
        } catch (e: AmqpException){
            logger.error("Failed to send temperature data to queue ${temperatureQueue.name}. Client failed with: ${e.message}")
        }
    }

    @Scheduled(fixedDelay = 30000)
    fun sendPressure(){
        try {
            val pressure = PressureRecord(id.toString(),weatherRepository.getPressure());
            logger.info("Sending $pressure to ${pressureQueue.name}")
            rabbitClient.convertAndSend(pressureQueue.name, jacksonObjectMapper().writeValueAsString(pressure))
        } catch (e: AmqpException){
            logger.error("Failed to send pressure data to queue ${pressureQueue.name}. Client failed with: ${e.message}")
        }
    }

}