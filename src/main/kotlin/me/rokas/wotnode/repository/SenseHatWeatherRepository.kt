package me.rokas.wotnode.repository

import com.diozero.api.I2CConstants
import com.diozero.devices.LPS25H
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository

@Repository
@ConditionalOnProperty(name= ["wot.platform"], havingValue = "pi")
class SenseHatWeatherRepository: WeatherRepository {

    val sensor = LPS25H(I2CConstants.CONTROLLER_1, 0x5c)
    override fun getTemperature(): Double {
        return sensor.temperature.toDouble()
    }

    override fun getPressure(): Double {
        return sensor.pressure.toDouble()
    }
}