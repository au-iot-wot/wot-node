package me.rokas.wotnode.repository

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository
import kotlin.random.Random


@Repository
@ConditionalOnProperty(name= ["wot.platform"], havingValue = "x86")
class MockWeatherRepository : WeatherRepository {

    override fun getTemperature(): Double {
        return Random.nextDouble(10.0, 25.0)
    }

    override fun getPressure(): Double {
        return Random.nextDouble(1000.0, 1100.0)
    }
}