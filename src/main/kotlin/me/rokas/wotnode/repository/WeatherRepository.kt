package me.rokas.wotnode.repository

interface WeatherRepository {

    fun getTemperature() : Double
    fun getPressure() : Double

}